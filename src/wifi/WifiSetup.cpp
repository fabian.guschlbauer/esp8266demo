#include <wifi/WifiSetup.h>

void WifiSetup::connectWifi() {
  WiFi.disconnect();
  WiFi.begin("ssid", "password");
  WiFi.mode(WIFI_AP_STA);
  int wifiConnectTimeout = 10;
  while (wifiConnectTimeout > 0 && WiFi.status() != WL_CONNECTED) {
    Serial.println("");
    wifiConnectTimeout--;
    delay(1000);
    Serial.println("Connecting ... ");
  }
  if(WiFi.status() != WL_CONNECTED) {
    Serial.println("Failed to connect to Wifi");
    Serial.println("Create Fallback Wifi Access Point!");
    WiFi.softAP("ap_ssid", "ap_password");
    Serial.println(WiFi.softAPIP()); 
  }
  else {
    Serial.println("WiFi connection successful");
    Serial.println(WiFi.localIP()); 
  }
}
