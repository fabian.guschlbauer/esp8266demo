#include <webserver/Webserver.h>
#include <LittleFS.h>
#include <ArduinoJson.h>

static AsyncWebServer server(80);

void Webserver::startWebserver() {
    // Initialize LittleFS
    if(!LittleFS.begin()){
        Serial.println("Error on LittleFS begin");
        return;
    }

    // provide index.html
    server.on("/", HTTP_GET, [&](AsyncWebServerRequest *request) { 
      Serial.print("Received request / from client with IP: ");
      Serial.println(request->client()->remoteIP());
      request->send(LittleFS, "/index.html", String(), false); 
    });

    // example post request
    server.onRequestBody([&](AsyncWebServerRequest *request, uint8_t *data, size_t len, size_t index, size_t total) {
      if (request->url() == "/postValue" && request->methodToString() == "POST") {
        DynamicJsonDocument doc(2048);
        deserializeJson(doc, data);
        String serializedBody;
        serializeJson(doc, serializedBody);
        Serial.print("Received request /postValue [REQUEST]\t\r\n");
        Serial.println(serializedBody);
        request->send(200);
      }
    });

    // start webserver
    server.begin();
    Serial.println("HTTP server started");
}