#include <Arduino.h>
#include <LittleFS.h>
#include <wifi/WifiSetup.h>
#include <webserver/Webserver.h>

static WifiSetup wifiSetup;
Webserver webserver;


void printAllStoredFilenames() {
    // Initialize LittleFS
    Serial.println("Found files on LittleFS");
    if(!LittleFS.begin()){
        Serial.println("Error on LittleFS begin");
        return;
    }
	
    Dir dir = LittleFS.openDir("/");
    while (dir.next()) {
        Serial.println(dir.fileName());
    }
}

// Blink microcontroler's built in led 
void setup() {

  // Sets the data rate in bits per second (baud) for serial data transmission. 
  // For communicating with Serial Monitor, make sure to use the correct baud rate for your microcontroller. 
  Serial.begin(9600);
  Serial.println("Starting up..");

  printAllStoredFilenames();
  
  // initialize digital pin LED_BUILTIN as an output.
  pinMode(LED_BUILTIN, OUTPUT);

  // Setup wifi connection
  wifiSetup.connectWifi();

  // Startup webserver;
  webserver.startWebserver();
  
}

void loop() {
                      
}